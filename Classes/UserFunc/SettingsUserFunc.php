<?php
namespace HIVE\HiveUserfuncs\UserFunc;

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface;

class SettingsUserFunc
{
    /**
     * @param $sPlugin
     * @return mixed
     */
    public static function getFullTyposcriptArrayForPlugin ($sPlugin) {
        $objectManager = GeneralUtility::makeInstance('TYPO3\\CMS\\Extbase\\Object\\ObjectManager');
        $configurationManager = $objectManager->get('TYPO3\\CMS\\Extbase\\Configuration\\ConfigurationManager');
        $aFullTyposcriptSettings = $configurationManager->getConfiguration(ConfigurationManagerInterface::CONFIGURATION_TYPE_FULL_TYPOSCRIPT);
        $aTypoScriptSettingsForPlugin = $aFullTyposcriptSettings['plugin.'][$sPlugin . '.'];
        return $aTypoScriptSettingsForPlugin;
    }

    /**
     * @param $sPlugin
     * @return mixed
     */
    public static function getCleanFullTyposcriptArrayForPlugin ($sPlugin) {
        $aSettings = self::getFullTyposcriptArrayForPlugin($sPlugin);
        $aTypoScriptSettingsForPlugin = self::stringReplaceTyposcriptKeysRecursive($aSettings);
        return $aTypoScriptSettingsForPlugin;
    }

    /**
     * @param array $array
     * @return array
     */
    public static function stringReplaceTyposcriptKeysRecursive(array $array)
    {
        $aResult = [];
        foreach ($array as $key => $value) {
            if (is_array($value)) {
                $aResult[str_replace(".", "", $key)] = self::stringReplaceTyposcriptKeysRecursive($value);
            } else {
                $aResult[$key] = $value;
            }
        }
        return $aResult;
    }

}